import java.util.*;
public class b{
  static Scanner sc = new Scanner(System.in);
  public static void main(String[] args) {
    long St = System.nanoTime();

    solve();

    // int T = inputArrayInt()[0];
    //
    // for(int TC=1; TC <= T; TC++){
    //   System.out.printf("Kasus #%d:\n",TC);
    //   solve();
    // }

    long En = System.nanoTime();
    // time(St, En);
  }

  static void solve(){
    int n = scI();
    int m = scI();
    char[][] grid = new char[n][m];
    for(int i=0; i<n; i++){
      grid[i] = scN().toCharArray();
    }

    for(int j=0; j<m; j++){
      int c = n-1;
      for(int i=n-1; i>=0; i--){
        if(grid[i][j] == '#'){
          grid[i][j] = '.';
          grid[c][j] = '#';
          c = c-1;
        }
      }
    }

    for(char[] c : grid){
      for(char cc : c){
        System.out.print(cc);
      }
      System.out.println();
    }
  }

  /*OWN METHOD*/

  static void time(long a, long b){
    System.err.printf("Time : %.3f ms\n", (double)(b-a)/1000000000);
  }

  static String scN(){
    return sc.next();
  }

  static String scLn(){
    return sc.nextLine();
  }

  static int scI(){
    return sc.nextInt();
  }

  static String[] scLSA(){
    return sc.nextLine().split(" ");
  }

  static int[] getIntArray(String[] a){
    int r[] = new int[a.length];
    int i = 0;
    for (String x : a) {
      r[i++] = Integer.parseInt(x);
    }
    return r;
  }

  static void showArrayInt(int[] a){
    for (int x : a) {
      System.out.println(x);
    }
  }

  static void showArrayString(String[] a){
    for (String x : a) {
      System.out.println(x);
    }
  }

  static int[] inputArrayInt(){
    return getIntArray(sc.nextLine().split(" "));
  }

  static int[] inputArrayInt(int n){
    int[] a = new int[n];
    for(int i=0; i<n; i++) a[i] = scI();
    return a;
  }

  /* END OF OWN METHOD*/

}
