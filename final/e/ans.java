import java.util.*;
public class ans{
  static Scanner sc = new Scanner(System.in);
  public static void main(String[] args) {
    long St = System.nanoTime();

    solve();

    // int T = inputArrayInt()[0];
    //
    // for(int TC=1; TC <= T; TC++){
    //   System.out.printf("Kasus #%d:\n",TC);
    //   solve();
    // }

    long En = System.nanoTime();
    time(St, En);
  }

  static int[] record;

  static void solve(){
    int m = scI();
    int[] a = new int[m];
    for(int i=0; i<m; i++){
      a[i] = scI();
    }
    int n = scI();
    record = new int[n+1];
    Arrays.fill(record, -1);

    System.out.println(f(a, n));
  }

  static int f(int[] a, int n){
    if(record[n] != -1) return record[n];
    if(n <= 0) return 0;
    int min = Integer.MAX_VALUE;

    for(int i=a.length-1; i>=0; i--){
      if(a[i] <= n){
        min = Math.min(min, f(a, n-a[i]) + 1);
      }
    }
    record[n] = min;
    return min;
  }

  /*OWN METHOD*/

  static void time(long a, long b){
    System.err.printf("Time : %.3f ms\n", (double)(b-a)/1000000000);
  }

  static String scLn(){
    return sc.nextLine();
  }

  static int scI(){
    return sc.nextInt();
  }

  static String[] scLSA(){
    return sc.nextLine().split(" ");
  }

  static int[] getIntArray(String[] a){
    int r[] = new int[a.length];
    int i = 0;
    for (String x : a) {
      r[i++] = Integer.parseInt(x);
    }
    return r;
  }

  static void showArrayInt(int[] a){
    for (int x : a) {
      System.out.println(x);
    }
  }

  static void showArrayString(String[] a){
    for (String x : a) {
      System.out.println(x);
    }
  }

  static int[] inputArrayInt(){
    return getIntArray(sc.nextLine().split(" "));
  }

  static int[] inputArrayInt(int n){
    int[] a = new int[n];
    for(int i=0; i<n; i++) a[i] = scI();
    return a;
  }

  /* END OF OWN METHOD*/

}
