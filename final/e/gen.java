import java.util.*;
public class gen {
  public static void main(String[] args) {
    ArrayList<Integer> list = new ArrayList<Integer>();
    int m = (int) (Math.random()*40) + 10;
    int[] a = new int[m];
    a[0] = 1;
    for(int i=1; i<m; i++){
      a[i] = (int) (Math.random()*100);
    }

    Arrays.sort(a);

    int curr = 0;
    for(int x : a){
      if(x != curr){
        list.add(x);
        curr = x;
      }
    }

    System.out.println(list.size());

    for(Integer x : list){
      System.out.println(x);
    }

    System.out.println((int) (Math.random()*1000));
  }
}
