import java.util.*;
public class ans{
  static Scanner sc = new Scanner(System.in);
  public static void main(String[] args) {
    long St = System.nanoTime();

    solve();

    // int T = inputArrayInt()[0];
    //
    // for(int TC=1; TC <= T; TC++){
    //   System.out.printf("Kasus #%d:\n",TC);
    //   solve();
    // }

    long En = System.nanoTime();
    time(St, En);
  }

  static void solve(){
    String s = sc.next();
    int a = scI();
    int b = scI();
    int c = scI();
    int d = scI();

    int res = -1;
    switch(s.charAt(0)){
      case 'K' : res = K(a,b,c,d);
      break;
      case 'Q' : res = Q(a,b,c,d);
      break;
      case 'B' : res = B(a,b,c,d);
      break;
      case 'R' : res = R(a,b,c,d);
    }

    System.out.println(res);
  }

  static int K(int a, int b, int c, int d){
    return Math.min(Math.abs(a-c), Math.abs(b-d)) + Math.abs(Math.abs(a-c) - Math.abs(b-d));
  }

  static int Q(int a, int b, int c, int d){
    if(Math.abs(a-c) == Math.abs(b-d)) return 1;
    if(a == c || b == d) return 1;
    return 2;
  }

  static int B(int a, int b, int c, int d){
    if(Math.abs(c-a) == Math.abs(d-b) && Math.abs(c-a) % 2 == Math.abs(d-b) % 2) return 1;
    if(Math.abs(c-a) != Math.abs(d-b) && Math.abs(c-a) % 2 == Math.abs(d-b) % 2) return 2;
    return -1;
  }

  static int R(int a, int b, int c, int d){
    if(a == c || b == d) return 1;
    return 2;
  }

  /*OWN METHOD*/

  static void time(long a, long b){
    System.err.printf("Time : %.3f ms\n", (double)(b-a)/1000000000);
  }

  static String scLn(){
    return sc.nextLine();
  }

  static int scI(){
    return sc.nextInt();
  }

  static String[] scLSA(){
    return sc.nextLine().split(" ");
  }

  static int[] getIntArray(String[] a){
    int r[] = new int[a.length];
    int i = 0;
    for (String x : a) {
      r[i++] = Integer.parseInt(x);
    }
    return r;
  }

  static void showArrayInt(int[] a){
    for (int x : a) {
      System.out.println(x);
    }
  }

  static void showArrayString(String[] a){
    for (String x : a) {
      System.out.println(x);
    }
  }

  static int[] inputArrayInt(){
    return getIntArray(sc.nextLine().split(" "));
  }

  static int[] inputArrayInt(int n){
    int[] a = new int[n];
    for(int i=0; i<n; i++) a[i] = scI();
    return a;
  }

  /* END OF OWN METHOD*/

}
