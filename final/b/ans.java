import java.util.*;
public class ans{
  static Scanner sc = new Scanner(System.in);
  public static void main(String[] args) {
    long St = System.nanoTime();

    solve();

    // int T = inputArrayInt()[0];
    //
    // for(int TC=1; TC <= T; TC++){
    //   System.out.printf("Kasus #%d:\n",TC);
    //   solve();
    // }

    long En = System.nanoTime();
    time(St, En);
  }

  static class Node{
    int start, end;
    public Node(int a, int b){
      start = a;
      end = b;
    }
  }

  static void solve(){
    int n = scI();
    Node[] node = new Node[n];

    for(int i=0; i<n; i++){
      int start = scI();
      int end = scI();
      node[i] = new Node(start, end);
    }

    for(int i=0; i<n; i++){
      int curr = i;
      for(int j=i+1; j<n; j++){
        if(node[curr].end > node[j].end){
          curr = j;
        }
      }
      Node temp = node[curr];
      node[curr] = node[i];
      node[i] = temp;
    }

    int res = 1;
    Node current = node[0];

    // for(int i=0; i<n;i++){
    //   System.out.println(node[i].start + " || " + node[i].end);
    // }

    // System.out.println("======================");

    for(int i=0; i<n; i++){
      if(node[i].start > current.end){
        // System.out.println(node[i].start + " || " + node[i].end);
        current = node[i];
        res++;
      }
    }

    System.out.println(res);

  }

  /*OWN METHOD*/

  static void time(long a, long b){
    System.err.printf("Time : %.3f ms\n", (double)(b-a)/1000000000);
  }

  static String scLn(){
    return sc.nextLine();
  }

  static int scI(){
    return sc.nextInt();
  }

  static String[] scLSA(){
    return sc.nextLine().split(" ");
  }

  static int[] getIntArray(String[] a){
    int r[] = new int[a.length];
    int i = 0;
    for (String x : a) {
      r[i++] = Integer.parseInt(x);
    }
    return r;
  }

  static void showArrayInt(int[] a){
    for (int x : a) {
      System.out.println(x);
    }
  }

  static void showArrayString(String[] a){
    for (String x : a) {
      System.out.println(x);
    }
  }

  static int[] inputArrayInt(){
    return getIntArray(sc.nextLine().split(" "));
  }

  static int[] inputArrayInt(int n){
    int[] a = new int[n];
    for(int i=0; i<n; i++) a[i] = scI();
    return a;
  }

  /* END OF OWN METHOD*/

}
