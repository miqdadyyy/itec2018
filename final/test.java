//https://codefights.com/challenge/J2KshRwM483QWTwYe

public class test{
  public static void main(String[] args) {
    int[][] map =
      {{1,0,0,2,2,0},
       {0,0,2,1,0,2},
       {0,1,1,2,2,2},
       {1,2,1,0,2,1}};
    System.out.println(largestPlateau(map));

  }

  static int[][] g;
  static int dx[] = {1,0,-1,0};
  static int dy[] = {0,1,0,-1};
  static int largestPlateau(int[][] gg) {
    g = gg;
    int res = 0;

    for(int i=0; i<g.length; i++){
      for(int j=0; j<g[i].length; j++){
        if(g[i][j] == -1) continue;
        int curr = 0;
        if(c(j,i,g[i].length, g.length)){
          curr = dfs(j,i, g[i].length, g.length, g[i][j]) + 1;
          res = Math.max(res, curr);
        }
        // for (int[] x : g) {
        //   for (int y : x) {
        //     System.out.print(y+" ");
        //   }
        //   System.out.println();
        // }
        // System.out.println("==========");
      }
    }

    return res;
  }

  static boolean c(int x, int y, int c, int r){
    return ((x >= 0 && x < c) && (y >= 0 && y < r));
  }

  static int dfs(int x, int y, int c, int r, int curr){
    if(!c(x,y,c,r)) return 0;
    if(g[y][x] == -1) return 0;
    if(g[y][x] != curr) return 0;

    g[y][x] = -1;

    int res = 0;

    for(int d=0; d<4; d++){
      int xx = x+dx[d];
      int yy = y+dy[d];
      if(c(xx,yy,c,r)){
        // System.out.println(g[yy][xx]);
        if(g[yy][xx] == curr){
          res += dfs(xx,yy,c,r,curr) + 1;
          // res += Math.max(dfs(xx,yy,c,r,curr) + 1, res);
        }
      }
    }


    return res;
  }
}
