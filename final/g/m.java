import java.util.*;
public class m{
  static Scanner sc = new Scanner(System.in);
  public static void main(String[] args) {
    long St = System.nanoTime();

    solve();

    // int T = inputArrayInt()[0];
    //
    // for(int TC=1; TC <= T; TC++){
    //   System.out.printf("Kasus #%d:\n",TC);
    //   solve();
    // }

    long En = System.nanoTime();
    time(St, En);
  }

  static void solve(){
    int n = scI();
    int m = scI();

    int[] dx = {1,1,0,-1,-1,-1,0,1};
    int[] dy = {0,1,1,1,0,-1,-1,-1};
    char[][] grid = new char[n][m];

    for(int i=0; i<n; i++){
      grid[i] = sc.next().toCharArray();
    }

    char[][] res = new char[n][m];
    for(int i=0; i<n; i++){
      for(int j=0; j<m; j++){
        if(grid[i][j] == '*'){
          res[i][j] = '*';
          continue;
        }
        int c = 0;
        for(int d=0; d<8; d++){
          int x = j+dx[d];
          int y = i+dy[d];
          if(isValid(x,y,m,n)){
            if(grid[y][x] == '*') c++;
          }
        }
        res[i][j] = (char)('0' + c);
      }
    }

    for (char[] a : res ) {
      System.out.println(a);
    }

  }

  static boolean isValid(int x, int y, int c, int r){
    return ((x>=0 && x<c) && (y>=0 && y<r));
  }

  /*OWN METHOD*/

  static void time(long a, long b){
    System.err.printf("Time : %.3f ms\n", (double)(b-a)/1000000000);
  }

  static String scLn(){
    return sc.nextLine();
  }

  static int scI(){
    return sc.nextInt();
  }

  static String[] scLSA(){
    return sc.nextLine().split(" ");
  }

  static int[] getIntArray(String[] a){
    int r[] = new int[a.length];
    int i = 0;
    for (String x : a) {
      r[i++] = Integer.parseInt(x);
    }
    return r;
  }

  static void showArrayInt(int[] a){
    for (int x : a) {
      System.out.println(x);
    }
  }

  static void showArrayString(String[] a){
    for (String x : a) {
      System.out.println(x);
    }
  }

  static int[] inputArrayInt(){
    return getIntArray(sc.nextLine().split(" "));
  }

  static int[] inputArrayInt(int n){
    int[] a = new int[n];
    for(int i=0; i<n; i++) a[i] = scI();
    return a;
  }

  /* END OF OWN METHOD*/

}
