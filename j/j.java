import java.util.*;
public class j{
  static Scanner sc = new Scanner(System.in);
  public static void main(String[] args) {
    long St = System.nanoTime();

    solve();

    // int T = inputArrayInt()[0];
    //
    // for(int TC=1; TC <= T; TC++){
    //   System.out.printf("Kasus #%d:\n",TC);
    //   solve();
    // }

    long En = System.nanoTime();
    time(St, En);
  }

  static void solve(){
    int n = scI();
    int[] mhs = new int[n];
    for(int i=0; i<n; i++){
      int a = scI();
      int b = scI();
      int c = scI();
      mhs[i] = a+b+c;
    }
    int weaboo = 0;
    for(int i=0; i<n; i++){
      if(mhs[i] >= 200) weaboo++;
    }
    int normal = n-weaboo;
    if(weaboo >= normal){
      System.out.println("Dih kelas bau bawang");
      System.err.println("Dih kelas bau bawang");
    }
    else{
      System.out.println("Alhamdulillah kelasnya normal");
      System.err.println("Alhamdulillah kelasnya normal");
    }
  }

  /*OWN METHOD*/

  static void time(long a, long b){
    System.err.printf("Time : %.3f ms\n", (double)(b-a)/1000000000);
  }

  static String scLn(){
    return sc.nextLine();
  }

  static int scI(){
    return sc.nextInt();
  }

  static String[] scLSA(){
    return sc.nextLine().split(" ");
  }

  static int[] getIntArray(String[] a){
    int r[] = new int[a.length];
    int i = 0;
    for (String x : a) {
      r[i++] = Integer.parseInt(x);
    }
    return r;
  }

  static void showArrayInt(int[] a){
    for (int x : a) {
      System.out.println(x);
    }
  }

  static void showArrayString(String[] a){
    for (String x : a) {
      System.out.println(x);
    }
  }

  static int[] inputArrayInt(){
    return getIntArray(sc.nextLine().split(" "));
  }

  static int[] inputArrayInt(int n){
    int[] a = new int[n];
    for(int i=0; i<n; i++) a[i] = scI();
    return a;
  }

  /* END OF OWN METHOD*/

}
