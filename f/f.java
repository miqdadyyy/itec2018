import java.util.*;
public class f{
  static Scanner sc = new Scanner(System.in);
  public static void main(String[] args) {
    long St = System.nanoTime();

    solve();

    // int T = inputArrayInt()[0];
    //
    // for(int TC=1; TC <= T; TC++){
    //   System.out.printf("Kasus #%d:\n",TC);
    //   solve();
    // }

    long En = System.nanoTime();
    time(St, En);
  }

  static void solve(){
    Stack stack = new Stack();
    int N = scI();
    int[] a = new int[N];
    for(int i=0; i<N; i++){
      a[i] = scI();
    }
    for(int i=N-1; i>=0; i--){
      stack.push(a[i]);
    }
    s(stack);
    System.out.println();
  }

  static void s(Stack s2){
    if(s2.size() == 1){
      System.out.print(s2.pop() + " ");
    }
    else if(s2.size() == 2){
      System.out.print(s2.pop() + " ");
      System.out.print(s2.pop() + " ");
    }
    else{
      Stack s1 = new Stack();
      Stack s3 = new Stack();
      int n = s2.size()/3;
      for(int i=0; i<n; i++){
        s1.push(s2.pop());
      }
      for(int i=0; i<n; i++){
        s3.push(s2.pop());
      }

      s(s1);
      s(s2);
      s(s3);
    }
  }

  /*OWN METHOD*/

  static void time(long a, long b){
    System.err.printf("Time : %.3f ms\n", (double)(b-a)/1000000000);
  }

  static String scLn(){
    return sc.nextLine();
  }

  static int scI(){
    return sc.nextInt();
  }

  static String[] scLSA(){
    return sc.nextLine().split(" ");
  }

  static int[] getIntArray(String[] a){
    int r[] = new int[a.length];
    int i = 0;
    for (String x : a) {
      r[i++] = Integer.parseInt(x);
    }
    return r;
  }

  static void showArrayInt(int[] a){
    for (int x : a) {
      System.out.println(x);
    }
  }

  static void showArrayString(String[] a){
    for (String x : a) {
      System.out.println(x);
    }
  }

  static int[] inputArrayInt(){
    return getIntArray(sc.nextLine().split(" "));
  }

  static int[] inputArrayInt(int n){
    int[] a = new int[n];
    for(int i=0; i<n; i++) a[i] = scI();
    return a;
  }

  /* END OF OWN METHOD*/

}
