Rulebook ITeC 2018 Programming

1. Peserta Login ke website yang disediakan panitia dan menggunakan akun yang telah dibuat panitia
2. Peserta masuk ke contest "Penyisihan ITeC 2018"
3. Contest dimulai pada tanggal 20 Juni 2018 pukul 10.00 hingga 13.00
4. Pada saat penyisihan perserta dapat menggunakan internet
5. Scoreboard dapat dilihat public
6. Pada penyisihan terdapat 15 soal
7. Peserta dilarang untuk berbagi jawaban dengan tim lain
8. Jika peserta terbukti berbagi jawaban dengan tim lain, maka kedua tim tersebut akan didiskalifikasi
9. Peserta dapat menggunakan bahasa C, C++, Python2, Python3 dan Java
10. 10 Tim teratas akan melanjutkan ke babak final
11. Time limit untuk babak penyisihan adalah 10 detik, jika waktu eksekusi melebihi 10 detik maka terdapat infinity loop pada program anda.
12. Untuk pengguna java peserta disarankan untuk tidak menggunakan nextLine(), terkecuali soal tertentu untuk menghindari bug.
13. Jika terdapat kesalahan pada soal atau sistem maka akan ada pengumuman di announcement.
14. 
